# ColludeMark

Collude with your friends in markdown! All you need is a git repository with markdown files and the editor creates a friendly place to collaborate and discuss with the markdown rendered right in front of you. Easily switch between the source markdown and the rendered view while still staying in the context of the discussion.

## In progress!

I'm just trying out this idea for now - still working out the design and what a prototype would even look like, hop in the issues section and let's figure this out together!

## Development

This project is bootstrapped with [Create Elm App](https://github.com/halfzebra/create-elm-app).

Below you will find some information on how to perform basic tasks.
You can find the most recent version of this guide [here](https://github.com/halfzebra/create-elm-app/blob/master/template/README.md).

In order to get anything done, make sure you have `Node`, `Elm`, and `Create Elm App` installed.

### Step by Step

Here is a step-by-step guide to getting the code running.

1. Install [NodeJS](https://nodejs.org/en/download/) (at least version 8)

    You need node and npm installed to get everything rolling - make sure you can do `node -v` and `npm -v`.

2. Install [Elm](https://guide.elm-lang.org/install.html)

    Elm has an excellent install story now! Run the install media or follow the instructions and verify that it works by running `which elm` or by trying to open the repl by using `elm repl`.

3. Install [Create Elm App](https://github.com/halfzebra/create-elm-app)

    You need this tool installed globally, and then you get to use some _super_ useful tooling.

4. Clone this repo

    `git clone https://gitlab.com/sNewell/collude-mark.git`


5. Run `elm-app start` in the root of the repo

    It should open up on `localhost:3000`.

### Installing Elm packages

```sh
elm-app install <package-name>
```

Other `elm-package` commands are also [available].(#package)

### Installing JavaScript packages

To use JavaScript packages from npm, you'll need to add a `package.json`, install the dependencies, and you're ready to go.

```sh
npm init -y # Add package.json
npm install --save-dev pouchdb-browser # Install library from npm
```

```js
// Use in your JS code
import PouchDB from 'pouchdb-browser';
const db = new PouchDB('mydb');
```

### Folder structure

```sh
my-app/
├── .gitignore
├── README.md
├── elm.json
├── elm-stuff
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo.svg
│   └── manifest.json
├── src
│   ├── Main.elm
│   ├── index.js
│   ├── main.css
│   └── registerServiceWorker.js
└── tests
    └── Tests.elm
```

For the project to build, these files must exist with exact filenames:

* `public/index.html` is the page template;
* `public/favicon.ico` is the icon you see in the browser tab;
* `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside src.

### Available scripts

In the project directory you can run:

#### `elm-app build`

Builds the app for production to the `build` folder.

The build is minified, and the filenames include the hashes.
Your app is ready to be deployed!

#### `elm-app start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

You may change the listening port number by using the `PORT` environment variable.

#### `elm-app install`

Alias for [`elm install`](http://guide.elm-lang.org/get_started.html#elm-install)

Use it for installing Elm packages from [package.elm-lang.org](http://package.elm-lang.org/)

#### `elm-app test`

Run tests with [node-test-runner](https://github.com/rtfeldman/node-test-runner/tree/master)

You can make test runner watch project files by running:

```sh
elm-app test --watch
```

#### `elm-app eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Elm Platform, etc.) right into your project, so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point, you’re on your own.

You don’t have to use 'eject' The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However, we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

#### `elm-app <elm-platform-command>`

Create Elm App does not rely on the global installation of Elm Platform, but you still can use its local Elm Platform to access default command line tools:

##### `repl`

Alias for [`elm repl`](http://guide.elm-lang.org/get_started.html#elm-repl)

##### `make`

Alias for [`elm make`](http://guide.elm-lang.org/get_started.html#elm-make)

##### `reactor`

Alias for [`elm reactor`](http://guide.elm-lang.org/get_started.html#elm-reactor)

### Turning on/off Elm Debugger

By default, in production (`elm-app build`) the Debugger is turned off and in development mode (`elm-app start`) it's turned on.

To turn on/off Elm Debugger explicitly, set `ELM_DEBUGGER` environment variable to `true` or `false` respectively.

### Dead code elimination

Create Elm App comes with an opinionated setup for dead code elimination which is disabled by default, because it may break your code.

You can enable it by setting `DEAD_CODE_ELIMINATION` environment variable to `true`

### Changing the base path of the assets in the HTML

By default, assets will be linked from the HTML to the root url. For example `/css/style.css`.

If you deploy to a path that is not the root, you can change the `PUBLIC_URL` environment variable to properly reference your assets in the compiled assets. For example: `PUBLIC_URL=./ elm-app build`.

### Changing the Page `<title>`

You can find the source HTML file in the `public` folder of the generated project. You may edit the `<title>` tag in it to change the title from “Elm App” to anything else.

Note that normally you wouldn’t edit files in the `public` folder very often. For example, [adding a stylesheet](#adding-a-stylesheet) is done without touching the HTML.

If you need to dynamically update the page title based on the content, you can use the browser [`document.title`](https://developer.mozilla.org/en-US/docs/Web/API/Document/title) API and JavaScript interoperation. The next section of this tutorial will explain it in more detail.

### JavaScript Interop

You can send and receive values to and from JavaScript using [ports](https://guide.elm-lang.org/interop/javascript.html#ports).

In the following example we will use JavaScript to write a log in the console, every time the state changes in outrElm app. To make it work with files created by `create-elm-app` you need to modify
`src/index.js` file to look like this:

```js
import { Elm } from './Main.elm';

const app = Elm.Main.init({
  node: document.getElementById('root')
});

app.ports.logger.subscribe(message => {
  console.log('Port emitted a new message: ' + message);
});
```

Please note the `logger` port in the above example, more about it later.

First let's allow the Main module to use ports and in `Main.elm` file please prepend `port` to the module declaration:

```elm
port module Main exposing (..)
```

Do you remember `logger` in JavaScript? Let's declare the port:

```elm
port logger : String -> Cmd msg
```

and use it to call JavaScript in you update function.

```elm
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Inc ->
            ( { model | counter = model.counter + 1}
            , logger ("Elm-count up " ++ (toString (model.counter + 1)))
            )
        Dec ->
            ( { model | counter = model.counter - 1}
            , logger ("Elm-count down " ++ (toString (model.counter - 1))))
        NoOp ->
            ( model, Cmd.none )
```

Please note that for `Inc` and `Dec` operations `Cmd.none` was replaced with `logger` port call which sends a message string to the JavaScript side.

### More info!

As linked above, [the latest version of this guide](https://github.com/halfzebra/create-elm-app/blob/master/template/README.md) has more details - but I thought I'd put some quick reference material here for ease of access.
